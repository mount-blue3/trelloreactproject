import { Container, Box, Button, Input, IconButton } from "@chakra-ui/react";
import { useState, useEffect } from "react";
import { FaTrashAlt } from "react-icons/fa";
import CheckLists from "./CheckLists";
import { getCardsData, addNewCard, deleteCard } from "../api.js";
const Cards = ({ listId }) => {
  const [newCardName, setNewCardName] = useState("");
  const [showInput, setShowInput] = useState(false);
  const [selectedCardId, setSelectedCardId] = useState(null);
  const [cards, setCards] = useState([]);
  const fetchCards = async () => {
    try {
      const cardsData = await getCardsData(listId);
      setCards(cardsData);
    } catch (error) {
      console.error(error);
    }
  };

  const handleNewCard = async () => {
    try {
      const newCardData = await addNewCard(listId, newCardName);
      setCards((prevCards) => [...prevCards, newCardData]);
      setNewCardName("");
      setShowInput(false);
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddCard = () => {
    setShowInput(true);
  };

  const handleDeleteCard = async (cardId) => {
    try {
      await deleteCard(cardId);
      setCards((prevCards) => prevCards.filter((card) => card.id !== cardId));
    } catch (error) {
      console.error(error);
    }
  };

  const handleCardClick = (cardId) => {
    setSelectedCardId(cardId);
  };

  const handleCloseModal = () => {
    setSelectedCardId(null);
  };
  useEffect(() => {
    fetchCards();
  }, [listId]);
  return (
    <>
      <Container>
        {cards.map((card) => (
          <Box
            key={card.id}
            p={4}
            borderWidth={1}
            borderRadius="md"
            boxShadow="md"
            backgroundColor="gray.200"
            width="300px"
            mb={4}
            mr={4}
            mt={4}
            onClick={() => handleCardClick(card.id)}
            cursor="pointer"
          >
            {card.name}
            <IconButton
              icon={<FaTrashAlt />}
              aria-label="Delete"
              onClick={(event) => {
                event.stopPropagation();
                handleDeleteCard(card.id);
              }}
              size="sm"
              variant="ghost"
              colorScheme="red"
              ml={40}
            />
            <CheckLists
              cardId={card.id}
              isOpen={selectedCardId === card.id}
              onClose={handleCloseModal}
            />
          </Box>
        ))}
        {showInput ? (
          <Box
            p={4}
            borderWidth={1}
            borderRadius="md"
            boxShadow="md"
            backgroundColor="gray.200"
            width="300px"
            mb={4}
            mr={4}
            mt={4}
          >
            <Input
              type="text"
              value={newCardName}
              onChange={(e) => setNewCardName(e.target.value)}
              placeholder="Enter a new card"
            />
            <Button onClick={handleNewCard} mt={4} backgroundColor="blue.400">
              + Add a card
            </Button>
          </Box>
        ) : (
          <Button mt={4} backgroundColor="blue.400" onClick={handleAddCard}>
            Add new card
          </Button>
        )}
      </Container>
    </>
  );
};

export default Cards;

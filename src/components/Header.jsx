import { Box, Button, Flex, Image } from "@chakra-ui/react";
import logo from "../logo/images.png";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <Flex
      as="header"
      align="center"
      justify="space-between"
      px={4}
      py={2}
      marginLeft={20}
      marginRight={20}
      marginTop={10}
      bg="#00008B"
    >
      <Link to="/">
        <Button bg="#ADD8E6" marginLeft={20}>
          Home
        </Button>
      </Link>

      <Box>
        <Image src={logo} alt="Trello logo" mr={800} h={12} />
      </Box>
    </Flex>
  );
};

export default Header;

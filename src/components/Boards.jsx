import { Container, Box, Flex, Button, Input } from "@chakra-ui/react";
import { useState, useEffect } from "react";
import { getBoardsData, postNewBoard } from "../api.js";
import bg from "../logo/bg.jpg";
import { Link } from "react-router-dom";

const Boards = () => {
  const [allBoards, setAllBoards] = useState([]);
  const [newBoardName, setNewBoardName] = useState("");
  const [showInput, setShowInput] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isError, setIsError] = useState(null);
  const fetchBoards = async () => {
    try {
      const boards = await getBoardsData();
      setIsLoading(false);
      setIsError(null);
      setAllBoards(boards);
    } catch (error) {
      setIsError(error.message);
      setIsLoading(false);
      console.error(error);
    }
  };

  const handleAddBoard = () => {
    setShowInput(true);
  };
  const handleCreateBoard = async () => {
    try {
      await postNewBoard(newBoardName);
      fetchBoards();
      setNewBoardName("");
      setShowInput(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchBoards();
  }, []);
  return (
    <Container maxW="full" centerContent marginTop={4}>
      {isError && <Box>{isError}</Box>}
      {isLoading && <Box>Loading...</Box>}
      <Flex
        flexWrap="wrap"
        alignItems="center"
        justify="flex-start"
        gap={4}
        px={4}
        py={2}
      >
        {allBoards &&
          allBoards.map((board) => (
            <Link key={board.id} to={`/board/${board.id}`}>
              <Box
                p={4}
                borderWidth={1}
                borderRadius="md"
                boxShadow="md"
                width="350px"
                height="200px"
                backgroundImage={bg}
                backgroundSize="cover"
              >
                <Box ml={30} fontWeight="bold">
                  {board.name}
                </Box>
              </Box>
            </Link>
          ))}
        {showInput ? (
          <Box
            p={4}
            borderWidth={1}
            borderRadius="md"
            boxShadow="md"
            width="350px"
            height="200px"
          >
            <Input
              placeholder="Enter board name"
              value={newBoardName}
              onChange={(e) => setNewBoardName(e.target.value)}
              mb={2}
            />
            <Button onClick={handleCreateBoard}>Create Board</Button>
          </Box>
        ) : (
          <Button backgroundColor="blue.400" onClick={handleAddBoard}>
            Add Board
          </Button>
        )}
      </Flex>
    </Container>
  );
};

export default Boards;

import { useState, useEffect } from "react";
import {
  Box,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Input,
  Button,
  IconButton,
} from "@chakra-ui/react";
import { FaTrashAlt } from "react-icons/fa";
import CheckListItem from "./CheckListItem";
import { getCheckListsData, addNewCheckList, deleteCheckList } from "../api.js";

const Checklists = ({ cardId, isOpen, onClose }) => {
  const [checklists, setChecklists] = useState([]);
  const [newChecklistName, setNewChecklistName] = useState("");
  const [showInput, setShowInput] = useState(false);
  const fetchChecklists = async (cardId) => {
    try {
      const checkLists = await getCheckListsData(cardId);
      setChecklists(checkLists);
    } catch (error) {
      console.error(error);
    }
  };

  const handleClose = () => {
    onClose();
  };

  const handleAddChecklist = async () => {
    try {
      const newChecklistData = await addNewCheckList(cardId, newChecklistName);
      setChecklists((prevChecklists) => [...prevChecklists, newChecklistData]);
      setNewChecklistName("");
      setShowInput(false);
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteCheckList = async (checkListId) => {
    try {
      await deleteCheckList(checkListId);
      setChecklists((prevCheckLists) =>
        prevCheckLists.filter((checkList) => checkList.id !== checkListId)
      );
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    fetchChecklists(cardId);
  }, [cardId]);

  return (
    <Modal isOpen={isOpen} onClose={handleClose}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Checklists</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          {checklists.map((checklist) => (
            <Box
              key={checklist.id}
              p={2}
              mb={2}
              borderWidth={1}
              borderRadius="md"
            >
              <Box fontWeight="bold">
                {checklist.name}
                <IconButton
                  icon={<FaTrashAlt />}
                  aria-label="Delete"
                  onClick={(event) => {
                    event.stopPropagation();
                    handleDeleteCheckList(checklist.id);
                  }}
                  size="sm"
                  variant="ghost"
                  colorScheme="red"
                  ml={60}
                />
              </Box>
              <CheckListItem checklist={checklist} cardId={cardId} />
            </Box>
          ))}
          {showInput ? (
            <Box p={2} mb={2} borderWidth={1} borderRadius="md">
              <Input
                type="text"
                value={newChecklistName}
                onChange={(e) => setNewChecklistName(e.target.value)}
                placeholder="Enter the name of the new checklist"
              />
              <Button
                onClick={handleAddChecklist}
                mt={2}
                backgroundColor="blue.400"
              >
                Add Checklist
              </Button>
            </Box>
          ) : (
            <Button
              backgroundColor="blue.400"
              onClick={() => setShowInput(true)}
            >
              Add New Checklist
            </Button>
          )}
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default Checklists;

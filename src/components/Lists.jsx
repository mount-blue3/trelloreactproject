import { Box, Flex, IconButton, Button, Input } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { FaTrashAlt } from "react-icons/fa";
import Cards from "./Cards";
import { getListsData, postNewList, deleteList } from "../api.js";

const Lists = () => {
  const { boardId } = useParams();
  const [lists, setLists] = useState([]);
  const [newListName, setNewListName] = useState("");
  const [showInput, setShowInput] = useState(false);
  const fetchLists = async () => {
    try {
      const allLists = await getListsData(boardId);
      setLists(allLists);
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddNewList = async () => {
    try {
      const newList = await postNewList(boardId, newListName);
      setLists((prevLists) => [...prevLists, newList]);
      setNewListName("");
      setShowInput(false);
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddList = () => {
    setShowInput(true);
  };

  const handleDeleteList = async (listId) => {
    try {
      await deleteList(listId);
      setLists((prevLists) => prevLists.filter((list) => list.id !== listId));
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    fetchLists();
  }, [boardId]);

  return (
    <Flex direction="row" flexWrap="wrap">
      {lists.map((list) => (
        <Box
          key={list.id}
          p={4}
          borderWidth={1}
          borderRadius="md"
          boxShadow="md"
          backgroundColor="gray.200"
          width="350px"
          mb={4}
          mr={4}
          mt={4}
        >
          {list.name}
          <IconButton
            icon={<FaTrashAlt />}
            aria-label="Delete"
            onClick={() => handleDeleteList(list.id)}
            size="sm"
            variant="ghost"
            colorScheme="red"
            ml={60}
          />
          <Cards listId={list.id} />
        </Box>
      ))}
      {showInput ? (
        <Box
          p={4}
          borderWidth={1}
          borderRadius="md"
          boxShadow="md"
          backgroundColor="gray.200"
          width="350px"
          mb={4}
          mr={4}
          mt={4}
        >
          <Input
            type="text"
            value={newListName}
            onChange={(e) => setNewListName(e.target.value)}
            placeholder="New List Name"
          />
          <Button backgroundColor="blue.400" onClick={handleAddNewList} mt={4}>
            Create List
          </Button>
        </Box>
      ) : (
        <Button mt={4} backgroundColor="blue.400" onClick={handleAddList}>
          Add new list
        </Button>
      )}
    </Flex>
  );
};

export default Lists;

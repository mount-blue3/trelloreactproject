import { useState, useEffect } from "react";
import { Box, Input, Button, Progress, IconButton } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import {
  getCheckListItemsData,
  addNewCheckListItem,
  deleteCheckListItem,
  updateChecklistItemState,
} from "../api.js";

const CheckListItem = ({ checklist, cardId }) => {
  const [checkListItems, setCheckListItems] = useState([]);
  const [newCheckListItemName, setNewCheckListItemName] = useState("");
  const [showInput, setShowInput] = useState(false);

  const fetchCheckListItems = async () => {
    try {
      const checkListItems = await getCheckListItemsData(checklist.id);
      setCheckListItems(checkListItems);
    } catch (error) {
      console.error(error);
    }
  };

  const handleAddChecklistItem = async () => {
    try {
      const newChecklistData = await addNewCheckListItem(
        checklist.id,
        newCheckListItemName
      );
      setCheckListItems((prevChecklists) => [
        ...prevChecklists,
        newChecklistData,
      ]);
      setNewCheckListItemName("");
      setShowInput(false);
    } catch (error) {
      console.error(error);
    }
  };

  const handleToggleChecklistItem = async (itemId) => {
    try {
      const itemIndex = checkListItems.findIndex((item) => item.id === itemId);
      const updatedItems = [...checkListItems];
      updatedItems[itemIndex].state =
        updatedItems[itemIndex].state === "complete"
          ? "incomplete"
          : "complete";
      setCheckListItems(updatedItems);
      await updateChecklistItemState(
        cardId,
        checklist.id,
        itemId,
        updatedItems[itemIndex].state
      );
    } catch (error) {
      console.error(error);
    }
  };

  const handleDeleteChecklistItem = async (itemId) => {
    try {
      await deleteCheckListItem(checklist.id, itemId);
      setCheckListItems((prevChecklists) =>
        prevChecklists.filter((item) => {
          return item.id !== itemId;
        })
      );
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchCheckListItems();
  }, [checklist.id]);

  const completedItems = checkListItems.filter(
    (item) => item.state === "complete"
  );
  const totalItems = checkListItems.length;
  const percentageComplete =
    totalItems > 0 ? (completedItems.length / totalItems) * 100 : 0;

  return (
    <>
      {totalItems > 0 && (
        <Progress value={percentageComplete} colorScheme="teal" mb={4} />
      )}
      {checkListItems.map((item) => (
        <Box key={item.id}>
          <input
            type="checkbox"
            checked={item.state === "complete"}
            onChange={() => handleToggleChecklistItem(item.id)}
          />
          <Box
            as="span"
            ml={2}
            textDecoration={item.state === "complete" ? "line-through" : "none"}
          >
            {item.name}
          </Box>
          <IconButton
            icon={<CloseIcon />}
            onClick={() => handleDeleteChecklistItem(item.id)}
            aria-label="Delete"
            size="xs"
            backgroundColor="transparent"
            ml={10}
          />
        </Box>
      ))}
      {showInput ? (
        <Box p={2} mb={2} borderWidth={1} borderRadius="md">
          <Input
            type="text"
            value={newCheckListItemName}
            onChange={(e) => setNewCheckListItemName(e.target.value)}
            placeholder="Enter the name of the new checklist"
          />
          <Button
            backgroundColor="blue.400"
            onClick={handleAddChecklistItem}
            mt={2}
          >
            Add Checklist Item
          </Button>
        </Box>
      ) : (
        <Button backgroundColor="blue.400" onClick={() => setShowInput(true)}>
          Add New Item
        </Button>
      )}
    </>
  );
};

export default CheckListItem;

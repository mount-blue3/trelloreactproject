import axios from "axios";

const apiKey = "d06d258990b9adde40f72831cd8518fe";
const apiToken =
  "ATTA6ed13e52a3d054290210025db0b56efcf1f9903bccca28494c8c8d4f55f273b7381BD5E4";

export const getBoardsData = async () => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/members/me/boards?fields=name,url&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while fetching boards:", error);
    throw error;
  }
};
export const postNewBoard = async (newBoardName) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/boards?name=${newBoardName}&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while adding new board", error);
    throw error;
  }
};
export const getListsData = async (boardId) => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/boards/${boardId}/lists?fields=name&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while fetching lists:", error);
    throw error;
  }
};
export const postNewList = async (boardId, newListName) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/boards/${boardId}/lists?name=${newListName}&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while adding new list:", error);
    throw error;
  }
};
export const deleteList = async (listId) => {
  try {
    const response = await axios.put(
      `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while delete the list:", error);
    throw error;
  }
};
export const getCardsData = async (listId) => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while fetching the cards:", error);
    throw error;
  }
};
export const addNewCard = async (listId, newCard) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/cards?idList=${listId}&name=${newCard}&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    setIsError(error.message);
    console.error("Error while adding the card:", error);
    throw error;
  }
};
export const deleteCard = async (cardId) => {
  try {
    const response = await axios.put(
      `https://api.trello.com/1/cards/${cardId}?closed=true&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while delete the card:", error);
    throw error;
  }
};
export const getCheckListsData = async (cardId) => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while fetching the checklists:", error);
    throw error;
  }
};
export const addNewCheckList = async (cardId, newChecklist) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/checklists?idCard=${cardId}&name=${newChecklist}&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while adding the checklist:", error);
    throw error;
  }
};
export const deleteCheckList = async (checkListId) => {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${checkListId}?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while delete the checklist:", error);
    throw error;
  }
};
export const getCheckListItemsData = async (checkListId) => {
  try {
    const response = await axios.get(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while fetching the checklist items:", error);
    throw error;
  }
};
export const addNewCheckListItem = async (checkListId, newCheckListItem) => {
  try {
    const response = await axios.post(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${newCheckListItem}&key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while adding the checklist item:", error);
    throw error;
  }
};
export const deleteCheckListItem = async (checkListId, itemId) => {
  try {
    const response = await axios.delete(
      `https://api.trello.com/1/checklists/${checkListId}/checkItems/${itemId}?key=${apiKey}&token=${apiToken}`
    );
    return response.data;
  } catch (error) {
    console.error("Error while delete the checklist item:", error);
    throw error;
  }
};
export const updateChecklistItemState = async (
  cardId,
  checklistId,
  checkItemId,
  newState
) => {
  try {
    console.log("newstate", newState);
    const response = await axios.put(
      `https://api.trello.com/1/cards/${cardId}/checklist/${checklistId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}`,
      {
        state: newState,
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error while updating checklist item state:", error);
    throw error;
  }
};



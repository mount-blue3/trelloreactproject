import React from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./components/Header";
import Boards from "./components/Boards";
import Lists from "./components/Lists";
import { Container } from "@chakra-ui/react";

const App = () => {
  return (
    <div className="main-container">
      <Header />
      <Container maxW="container.xl">
        <Routes>
          <Route path="/" element={<Boards />} />
          <Route path="/board/:boardId" element={<Lists />} />
        </Routes>
      </Container>
    </div>
  );
};

export default App;
